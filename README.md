A white paper in response to a Request For Information on science applications that require exascale computing resources.

http://grants.nih.gov/grants/guide/notice-files/NOT-GM-15-122.html

Topics: Core-collapse supernovae, Binary Neutron Star Mergers, and Galaxy Formation and Chemical Evolution.

Submitted! On 10-16.

Co-signees:
S.M. Couch,
B.W. O'Shea,
C.D. Ott,
L.F. Roberts,
F.X. Timmes