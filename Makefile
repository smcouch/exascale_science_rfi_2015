NAME=exaHeavyMetal

LATEX=pdflatex 

FILES = exaHeavyMetal.tex 

all:  $(NAME).pdf $(FILES)

dvi: $(NAME).tex
	$(LATEX) $(NAME) || true
	#bibtex $(NAME)
	#$(LATEX) $(NAME) || true 
	#bibtex $(NAME)
	$(LATEX) $(NAME) || true 
	$(LATEX) $(NAME)

$(NAME).pdf: $(NAME).tex $(FILES)
	$(LATEX) $(NAME) || true
	#bibtex $(NAME)
	#$(LATEX) $(NAME) || true 
	#bibtex $(NAME)
	#$(LATEX) $(NAME) || true 
	#$(LATEX) $(NAME) || true 
	rm -f $(NAME).blg $(NAME).log $(NAME).aux

clean:
	rm -f *~ *.aux *.dvi *.gz *.out *.toc *log *-temp.* *.blg *~ $(NAME).ps $(NAME).bbl $(NAME).pdf


