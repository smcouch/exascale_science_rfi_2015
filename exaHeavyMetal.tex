\documentclass[11pt]{article}
\usepackage{geometry}
\geometry{letterpaper}
%\usepackage[parfill]{parskip}    % Activate to begin paragraphs with an empty line rather than an indent
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{epstopdf}
\usepackage{wrapfig}
%\usepackage{natbib}
\usepackage[dvipsnames]{xcolor}
\usepackage[square,comma,numbers]{natbib}
\usepackage[pdftex, plainpages=false, colorlinks=true, linkcolor=blue, citecolor=blue, bookmarks=false,urlcolor=blue]{hyperref}
\usepackage{setspace}
\usepackage{multicol}
\usepackage{sectsty}
\usepackage{url}
\usepackage{lipsum}
\usepackage{times}
\usepackage[tiny,compact]{titlesec}
\usepackage{fancyhdr}
%\usepackage{deluxetable}
\usepackage[font=footnotesize,labelfont=bf]{caption}
\usepackage{verbatim}
\usepackage{acronym}
\usepackage{comment}
\usepackage{soul}

\setlength{\textwidth}{6.5in}
\setlength{\oddsidemargin}{0.0cm}
\setlength{\evensidemargin}{0.0cm}
\setlength{\topmargin}{-0.5in}
\setlength{\headheight}{0.2in}
\setlength{\headsep}{0.2in}
\setlength{\textheight}{9.in}
%\setlength{\footskip}{-0.2in}
%\setlength{\voffset}{0.0in}


\sectionfont{\normalsize}
\subsectionfont{\normalsize}
\subsubsectionfont{\normalsize}
\singlespacing

\newcommand{\cdo}[1]{{\color{blue}$\blacksquare$~\textsf{[CDO: #1]}}}
\newcommand{\lfr}[1]{{\color{green}$\blacksquare$~\textsf{[LFR: #1]}}}

\pagestyle{fancy}
\fancyhf{} 
\rhead{\fancyplain{}{Synthesis of the Elements}}
\lhead{\fancyplain{}{In Response to NOT-GM-15-122}}
\rfoot{\fancyplain{}{\thepage}}

\begin{document}

\begin{center} 
    {\Large \bf The Cosmic Synthesis and Evolution of the Elements of Life} \\
    {\bf Sean M. Couch\footnotemark[1], Brian W. O'Shea\footnotemark[1], Christian D. Ott\footnotemark[2], Luke F. Roberts}\textsuperscript{1,2}{\bf, F.X. Timmes\footnotemark[3]}
\end{center}
\footnotetext[1]{Michigan State University}
\footnotetext[2]{California Institute of Technology}
\footnotetext[3]{Arizona State University}

Understanding the creation of the elements beyond hydrogen and helium is the central theme of nuclear astrophysics.
These elements are principally forged in stars, and are expelled into space by their subsequent explosions.
Several critical questions about cosmic nucleosynthesis remain unanswered, however.
What is the precise mix of heavy elements synthesized in stars of varying mass and metallicity?
How is this mix affected by uncertainties in, e.g., key nuclear reaction and weak interaction rates?
What is the mechanism that drives powerful supernova explosions at the ends of the lives of massive stars, spreading the synthesized elements into the space between the stars?
Which massive stars explode as supernovae, leaving behind ultra-dense neutron stars?
And which collapse into black holes?
What is the physical explanation for the observational connection between supernovae and gamma-ray bursts?
What is the production site of the very neutron rich ``{\it r-process}'' elements such as the lanthanides and actinides? 
%Can these r-process elements be produced in supernova explosions of massive stars, or are violent mergers of pairs of neutron stars required instead?
How are the elements produced by stars mixed into the surrounding medium of their host galaxies?
How do supernova explosions influence the regulation of star formation in their host galaxies and the expulsion of gas into the intergalactic medium?

Exploration of these questions is central to the missions of major Federally-funded experimental programs such as the Facility for Rare Isotope Beams, the Joint Institute for Nuclear Astrophysics, the Large Synoptic Survey Telescope, Advanced LIGO, the Dark Energy Survey, and various neutrino detector experiments.
These experimental efforts, however, would fail in their missions if not for concurrent synergistic theoretical research that brings vital physical understanding to the experimental data.
Capability computing is crucial to making more than just evolutionary advancement in this area, but current state-of-the-art simulations at the largest computational scales cannot adequately address cosmic chemical evolution.
Theoretical work on this topic falls into three broad classes addressing separate astrophysical phenomena: core-collapse supernova explosions, binary neutron star and black hole-neutron star mergers, and cosmological galaxy formation and evolution.
Each of these topics are extreme cases of multiscale, multiphysics problems and, despite the enormous differences in scales and critical physics, these three problems share many computational requirements.
Our understanding of the creation of the elements throughout the cosmos could be revolutionized by a concerted effort to develop a common exascale-capable software infrastructure for simulating core-collapse supernovae, binary neutron star mergers, and cosmological galaxy formation and evolution.
Such an effort is urgently needed if the research community is to be ready to address these critically-important questions at the exascale.

%At present, significant progress in each of these subdomains is severely hampered by the lack of sufficient computational resources.
%The petascale is simple insufficient to address these problems with the breadth and fidelity to adequately answer the fundamental questions of how the heavy elements were created and spread throughout the universe, dramatically altering the formation and evolution of galaxies, stars, and planets.

%Supernova and binary neutron star simulations share many common physics requirements: compressible magnetohydrodynamics, general relativistic gravitation, transport of neutrinos through semi-transparent media, explosive nuclear kinetics, and knowledge of the thermodynamic response of finite temperature material at densities exceeding that of atomic nuclei.
%Galaxy formation and evolution is the {\it the} quintessential multiscale computational problem requiring simulation boxes that span appreciable fractions of the observable universe with resolution fine enough to capture the dynamics of individual small galaxies.
%This dynamic range must be achieved while simulating crucial physics such as self-gravitating dark matter particles, compressible magnetohydrodynamics, radiative cooling, chemical evolution, and feedback mechanisms such as star formation, supernova explosions, and active galactic nuclei.

%: extremely multiscale magnetohydrodynamics capable of capture both shocks and turbulence tightly coupled to a dynamic (neutrino) radiation field, time-dependent (general relativistic) self-gravity, active and passive particles, arbitrary equations of state, and the capability to treat microphysical source terms accurately.
%Additionally, much of the methods and algorithms to address these problems with high-fidelity are well known, what is lacking is the ability to scale up to the level of computational power needed.


%Such an infrastructure would include a scalable adaptive mesh refinement framework, task-based parallelism, reliance on non-blocking asynchronous communication patterns, heavy reliance on accelerator technologies and handling of heterogenous on-node parallelism, intelligent memory management within a heterogenous memory hierarchy, and efficient handling of active and passive particle data structures.
%Additionally, in order to address the specific problems of core-collapse supernovae, binary neutron star mergers, and galaxy formation, this framework would require high-order accurate radiation magnetohydrodynamics, evolving spacetime general relativity, n-body gravitational dynamics, large nuclear and chemical networks fully coupled to the underlying gas dynamics, and seamless integration of local source terms generated by crucial physics.

%


{\bf The Need for Exascale.} 
Simulations of core-collapse supernovae, binary neutron star mergers, and galaxy evolution each require 3D shock-capturing magnetohydrodynamics, self-gravity (either Newtonian or general relativistic), accurate radiation transport (either for neutrinos or photons), and detailed nuclear/chemical kinetic networks.
These problems require enormous dynamic ranges in both space and time, generally necessitating adaptive mesh refinement techniques.
Additionally, supernova and neutron star mergers must use a complex microphysical equation of state and galaxy evolution demands treatment of microphysical plasma processes (including chemistry, heating, and cooling), cosmic rays, and prescriptive models for star and black hole formation and feedback. 
The current highest-fidelity simulations in these domains can require as much as 100 million core-hours {\it per simulation} on Leadership-class computational platforms (e.g., Titan, Mira, Blue Waters). 
Yet, these simulations are woefully under-resolved while neglecting magnetic fields and making some severe approximations to the radiation physics, microphysics, and gravity.
More than an order of magnitude increase in spatial resolution will be needed in all of these problems.
Such simulations will easily require 100 times the currently available computational resources.
Additionally, large suites of these simulations exploring the allowed parameter spaces will be needed to rigorously quantify the uncertainty in our theoretical models and compare them to observational and experimental data in a statistically meaningful way.

\begin{comment}
Simulating core-collapse supernovae or binary neutron star mergers in 3D with detailed neutrino radiation hydrodynamics, a microphysical equation of state, and a limited nuclear kinetic network requires roughly 100 million core-hours {\it per simulation} on current Leadership-class computational platforms (e.g., Titan, Mira, BlueWaters). \cdo{perhaps say that current codes already have problems to use resources efficiently at the petascale}
Yet, these simulations are woefully under-resolved while neglecting magnetic fields and making some severe approximations to the neutrino physics, nuclear microphysics, and gravity.
What is required are magnetohydrodynamic simulations with sufficient resolution to accurately capture the behavior of turbulence and crucial phenomena such as the magnetorotational instability with high-fidelity neutrino transport, large nuclear networks, and general relativity.  
More than an order-of-magnitude increase in spatial resolution will be needed.
Such simulations will easily require 100 times the currently available computational resources.
Additionally, current capability allows for only one or a few 3D high-fidelity supernova simulations. \cdo{in what timeframe? codes ready? just beginning to be ready, right?}
In order to address cosmic nucleosynthesis in a realistic way, we need whole suites of such simulations covering an appreciable range in stellar mass and metallicity, allowing us to compare theoretical predictions to observations and experiment in a statistically meaningful way.

Realistic modeling of galaxy formation and evolution requires extensive physics -- gravity, magnetohydrodynamics, radiation transport, microphysical plasma processes (including chemistry, heating, and cooling), cosmic rays, and prescriptive models for star and black hole formation and feedback.  
Furthermore, given the nature of cosmological structure formation it is necessary to model many thousands of galaxies over the entire age of the universe in order to realistically compare to observational data.
Taken together, these requirements result in an incredible computational challenge -- easily consuming millions of core-hours \textit{per simulation} on current Leadership-class platforms, for simulations that are under-resolved in both mass and space by factors of 10 to 100.
At least an order of magnitude increase in resolution is required, as well as the inclusion of more sophisticated models of radiation transport than are currently practical -- requiring a roughly two orders of magnitude increase in the computational expense \textit{per simulation}, with multiple simulations needed to adequately explore variations due to poorly-understood subgrid and microphysical models.  
\end{comment}

%Furthermore, the need to create synthetic observations via even more sophisticated radiation transport models (i.e., Monte Carlo transport including dust, polarization, etc.) will substantially add to the computational cost, particularly when dealing with many data outputs \cdo{data outputs -- is this just storage/ I/O or a postprocessing issue?} over the age of the universe.

{\bf Challenges of Reaching Exascale.}
Simulation of cosmic chemical evolution requires an enormous amount of memory and communication per FLOP.
For many reasons, current codes have trouble scaling even to current petascale resources.
These codes have typically been designed for traditional homogenous distributed-memory supercomputers within the paradigm that increase in capability will come both from advances in per-core performance and from concurrency.
These codes currently under-utilize accelerators while relying on highly synchronous communication patterns.
Additionally, cosmic chemical evolution simulations tax current parallel filesystems with frequent outputs and total data sizes of 100s of terabytes or more.
An exascale computer will be so dramatically different in its fundamental design (e.g., lower per-core performance/memory, heavy reliance on accelerators, concurrent thread counts of millions or billions, etc.) that it is highly unlikely that any existing software infrastructure used for computational astrophysics can be adapted to run efficiently at the exascale.
Thus, a concerted multidisciplinary effort is urgently needed to develop and exascale-capability multiscale multiphysics software infrastructure for general astrophysical simulation.

{\bf Potential Impact.}
Exascale simulation of cosmic chemical evolution will deliver reliable answers to crucial questions such as: How did the elements beyond hydrogen and helium form? What is the site of the r-process where very heavy neutron rich elements are formed? How did those elements pollute the universe with the basic ingredients for planets and life?
This will provide crucial insight, which can help shape the thrust of current and future experimental efforts such as aLIGO, FRIB, JINA, LSST, DES, and many others.
These experimental efforts are sure to make groundbreaking discoveries about cosmic chemical evolution, but exascale simulation will be critical to bringing physical understanding to those discoveries.

%An effort to overcome the technical challenges of scaling these simulations to the exascale will yield benefits for other domains which rely on similar computational and mathematical methods.
%Additionally, an exascale multiphysics, multiscale computational framework capable of end-to-end simulation of cosmic chemical evolution will have utility to an enormous range of other science problems within astrophysics and without.

{\bf Potential Benefit to Other Fields.}
Simulation of cosmic chemical evolution requires the multiscale, multiphysics solution of coupled hyperbolic, parabolic, and elliptical partial differential equations with both explicit and implicit methods.
A software infrastructure capable of exascale simulation of these problems would necessarily benefit many other domains including climate modeling, turbulent combustion, aerodynamics, fluid dynamics, solid mechanics, reactor design, cell biology, as well as many astrophysical topics including thermonuclear supernovae, star formation, planet formation, plasma (astro)physics, and stellar evolution.

{\bf Parameters of the Problem in 2025.}
Simulation of cosmic chemical evolution in ten years time will include the need to fully utilize accelerator technologies, higher-order/higher-fidelity numerical algorithms (i.e, more FLOPs per byte of memory), task-based parallelism, asynchronous communication, intelligent problem-aware methods for load balancing across millions or billions of independent threads of execution, and fault tolerance/resilience.
A software infrastructure capable of exascale astrophysical simulation will need to deftly handle complex intra-node and on-core memory hierarchies to maximize the amount of computation done per amount of data moved.
Additionally, exascale astrophysical simulations will produce petabytes of output data.
It is entirely possible that in certain, or even many, situations at the exascale traditional post-processing of data output in order to address target scientific questions will be infeasible due to the need for excessive data storage or read/write times.
Thus, scientific progress at the exascale for astrophysical problems may need to rely heavily on {\it in-situ} data analysis, necessitating extremely careful planning of required simulation diagnostics.
Extensive hardware and software infrastructure will be needed for storing, accessing, analyzing, and archiving exascale astrophysical simulation data.

{\bf Foundational Issues.}
Exascale simulation of cosmic chemical evolution will require a long term well-supported effort to develop and maintain a common multiscale, multiphysics simulation infrastructure.
Cross-disciplinary collaboration in the development and maintenance of such an infrastructure should be made a high priority in order to reduce costs associated with redundant effort and because such a simulation framework could be utilized by a wide range of domains beyond astrophysics.
A well-trained expert workforce capable of maintaining this framework, and executing exascale simulations with it, is also crucial.


\begin{comment}


introducing free parameters and unquantified uncertainty.

The physics is solved(ish):
We know how to do full GR.
We know how to do high-fidelity hydro and MHD.
We know how to do high-fidelity neutrino transport (microphysics still uncertain...).
We know how to do cosmology simulations (LCDM is very successful!).
We can model feedback/subgrid physics.

What we can't yet do is overcome the technical challenges:
Enormous resolution with full-fidelity physics.
Particles, gravity, transport, scaling to millions of threads, etc.
Uncertainty quantification (needs lots of models!)

Life/planets

r-process

FRIB

Uncertainty quantification

`` identifying scientific research that would benefit from a greatly enhanced new generation of HPC''

\section{Technical}

100x computing power of today

data analytics (big data, data science, CMSE)

``answer more complex multispatial, multiphysics scientific questions with more realism''

Will benefit other domains (engineering: solve elliptic PDEs, etc.)

``deriving fundamental understanding from large scale analytics''

\section{Questions from RFI}

``The specific scientific and research challenges that would need the projected 100-fold increase in application performance over what is possible today.''

Simulating CCSNe with full transport (VET, MC, energy-coupling, GR), GR gravity (maybe CFC, maybe EP), high-resolution MHD to capture turbulence.  
Detailed microphysics (perhaps computed on the fly), full in-situ nuclear networks (no assumption of equilibrium).
Need full range of progenitor masses and exploration of possible sources of stochasticity.

Simulating NS-NS mergers in full GR with detailed microphysics and neutrino tranpsort for a wide range of mass ratios, spins, tilts, etc.
GW extraction.

Simulating galaxy formation and evolution with 50 kpc resolution in a cubic Gpc volume.
Full chemical network, MHD, enormous dynamic range, radiation-hydro, physical feedback mechanisms (SNe, AGNe).

``The potential impact of the research to the scientific community, national economy, and society.''

Where did the elements come from? How did they mix from SN explosions into subsequent generation of stars?  
Where did the nuclei relevant to FRIB come from?  
What uncertainties in nuclear data (accessible to FRIB) are most important for understanding the evolution of the elements.

``The specific limitations/barriers of existing HPC systems must overcome to perform studies in this area.  Your comment can also include the level of performance on current architectures, and the projected increase in performance that is needed from future architectures.''

Scaling.  This is a software problem, largely.
Raw machine size.  Bigger machine, bigger problems.
Architectures not ideally suited to problems (?).

``Any related research areas you foresee that would benefit from this level of augmented computational capability. Identification of any barriers in addition to computational capability that impact the proposed research can also be considered.''

Engineering.

``Important computational and technical parameters of the problem as you expect them to be in 10 years (2025).  In addition to any specialized or unique computational capabilities that are required and/or need to be scaled up for addressing this scientific problem, e.g., in the areas of computing architectures, systems software and hardware, software applications, algorithm development, communications, and networking.''

Enormous zone counts.  
Enormous nuclear networks. 
Enormously expensive high-fidelity radiation transport.
Enormously expensive gravity (GR).
Lots of communication.

``Alternative models of deployment and resource accessibility arising out of exascale computing. Improvements in scientific workflow as well as particular requirements that may be needed by specific domains.''

``Capabilities needed by the end-to-end system, including data requirements such as data analytics and visualization tools, shared data capabilities, and data services which includes databases, portals and data transfer tools/nodes.''

Huge amounts of data and I/O.
Approaches for data analysis and processing at these scales.
Data curation.

``Foundational issues that need to be addressed such as training, workforce development or collaborative environments.''

We need people!
Grads, postdocs, research/technical professionals/engineers.

``Other areas of relevance for the Agencies to consider.''
\end{comment}


\end{document}
